import json
import logging
import os
import time

import requests

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.ERROR)

KEY = os.getenv('KEY')

BASE_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json?key=%s" % KEY

QUERY = "%(base_url)s&query=%(kind)s+center+in+%(city)s"

KINDS = [
    'teen',
    'senior',
    'community',
]

CITIES = [
    'Campbell',
    'Cupertino',
    'Gilroy',
    'Los Altos',
    'Los Altos Hills',
    'Los Gatos',
    'Milpitas',
    'Monte Sereno',
    'Morgan Hill',
    'Mountain View',
    'Saratoga',
    'Sunnyvale',
]

def main():
    queries = []
    results = {}
    for city in CITIES:
        if not city in results.keys():
            results[city] = {}
        for kind in KINDS:
            if not kind in results[city].keys():
               results[city][kind] = []
            query = QUERY % {
                'base_url': BASE_URL,
                'kind': kind,
                'city': city,
            }
            queries.append((city, kind, query))

    for city, kind, query in queries:
        response = requests.get(query)
        data = response.json()
        for r in data['results']:
            n = r.get('name')
            l = r.get('geometry').get('location')
            results[city][kind].append({'name': n, 'location': l})
            print(n, l)
        time.sleep(1)
    with open('results.json', 'w') as f:
        json.dump(results, f)

if __name__ == '__main__':
    main()
