#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""RESILIENT FICUS: Storage & Resilience Estimation Tool"""
import pandas as pd
import datetime as dt
from dateutil import parser
import matplotlib.pyplot as plt

TIMEZONE_HOURS = -8
PEAK_START = 16 + TIMEZONE_HOURS
PEAK_END = 22 + TIMEZONE_HOURS
BLDG_PEAK_KWH_PERCAP = 5
PERSON_DAILY_KWH_FOOTPRINT = 1

def readable_time(uxtimestr):
    uxtime = int(uxtimestr)
    mydt = dt.datetime.fromtimestamp(uxtime).strftime('%Y-%m-%d %H:%M')
    return mydt


def load_bulk():
    bulk500 = pd.read_csv("bulk_intervals_500.csv")
    return bulk500


def convert_bulk_to_dt(intvldf):
    startdt = intvldf['start'].apply(readable_time)
    istart = pd.to_datetime(startdt)
    intvldf['start'] = istart
    return intvldf


def get_tou_summaries(intvldf):
    daystart = parser.parse('06:00').time()
    dayend = parser.parse('18:00').time()
    dayhours = dayend.hour - daystart.hour
    nighthours = 24 - dayhours
    # bulk500 = pd.read_csv("bulk_intervals_500.csv")
    # bulk50 = copy.deepcopy(bulk500.head(50))
    # rb50 = process(bulk50)
    intvlday = intvldf[intvldf['start'].dt.time > daystart]
    intvlday = intvlday[intvldf['start'].dt.time <= dayend]
    intvlnight1 = intvldf[intvldf['start'].dt.time <= daystart]
    intvlnight2 = intvlday[intvldf['start'].dt.time > dayend]
    intvlnight = pd.concat([intvlnight1, intvlnight2], axis=0)
    sumday = dayhours * intvlday.groupby('meter_uid')['kwh'].mean()
    sumnight = nighthours * intvlnight.groupby('meter_uid')['kwh'].mean()
    sumdf = pd.DataFrame({"day": sumday, "night": sumnight})
    return sumdf


def get_24(intvldf, meter_uid):
    mdf = intvldf[intvldf['meter_uid'] == meter_uid]
    mdf.set_index(mdf['start'], inplace=True)
    md24 = mdf.groupby(mdf.index.hour)['kwh'].mean()
    md24.index = (md24.index + TIMEZONE_HOURS) % 24
    return md24


def plot_24(md24, cuid):
    plt.bar(md24.index, md24)
    plt.title("Meter ID: " + str(cuid))
    plt.show()


def get_community_sites(intvldf, sumdf, minkwh=8, plot=False):
    ''' Currently based on load heuristics.  The utility can provide real sites instead.'''
    comdf = sumdf[sumdf['day'] > 0.8 * sumdf['night']]
    comdf = comdf[comdf['night'] > minkwh]
    com_uids = comdf.index
    comsites = []
    for cuid in com_uids:
        md24 = get_24(intvldf, cuid)
        if max(md24) < 1.5 * min(md24):
            continue
        comsites += [cuid]
        if plot:
            plot_24(md24, cuid)
    return comsites


def estimate_storage_method1(intvldf, sites):
    sites = pd.Series(sites)
    storkwhs = sites.apply(lambda site: estimate_site_storage(intvldf, site))
    estdf = pd.DataFrame({"Site": sites, "Storage kWh": storkwhs})
    estdf.set_index('Site', inplace=True)
    return estdf


def estimate_site_storage_method1(intvldf, site, loadshiftpct=25, resilpct=50):
    # We apply a goal of shifting a percent of the 4pm-9pm load.
    # Also, we would like to reserve 50% of the energy storage for resilience
    md24 = get_24(intvldf, site)
    peakload = md24[PEAK_START:PEAK_END].sum()
    loadshiftkwh = peakload * loadshiftpct / 100
    storagekwh = loadshiftkwh * (100 - resilpct) / 100

    if storagekwh < 0:
        plot_24(md24, site)
    return storagekwh


def estimate_site_storage_method2(intvldf, site, numdays=7):
    '''
    We apply an estimate for energy requirement in an emergency
    * Estimated nunber of people the building can hold based on per-capita usage
    * Estimated daily kWh footprint of an additional person during an emergency
    '''
    md24 = get_24(intvldf, site)
    pplcapcty = int(md24.sum() / BLDG_PEAK_KWH_PERCAP)
    pplkwh = numdays * pplcapcty * PERSON_DAILY_KWH_FOOTPRINT
    loadshiftkwh = estimate_site_storage_method1(intvldf, site, resilpct=0)
    totalkwh = pplkwh + loadshiftkwh
    return pplcapcty, totalkwh


def estimate_storage_method2(intvldf, sites, numdays=7):
    people = []
    storkwh = []
    sites = pd.Series(sites)
    for site in sites: 
        peoplesite, storkwhsite = estimate_site_storage_method2(intvldf, site, numdays)
        people += [peoplesite]
        storkwh += [storkwhsite]
    estdf = pd.DataFrame({"Site": sites,
                          "People Capacity": people,
                          "Storage kWh": storkwh,
                          "Days": numdays})
    estdf.set_index('Site', inplace=True)
    return estdf


def estimate_storage(intvldf, sites, method=2):
    if method == 1:
        return estimate_storage_method1(intvldf, sites)
    elif method == 2:
        return estimate_storage_method2(intvldf, sites)
    return None


def plot_summaries(sumdf):
    fig, ax = plt.subplots()
    ax.scatter(sumdf['day'], sumdf['night'])
    for index, row in sumdf.iterrows():
        ax.annotate(index, [row['day'], row['night']])
    plt.plot([0, 50], [0, 50])
    plt.title("Day kWh vs. Night kWh per day")
    plt.xlabel("Day kWh (6am-6pm)")
    plt.ylabel("Night kWh (6pm-6am)")
    plt.show()
    

if __name__ == "__main__":
    # Load the bulk data into Pandas
    intvldf = load_bulk()

    # Convert Unix epochs to Pandas date/time
    intvldf = convert_bulk_to_dt(intvldf)

    # Get day vs. night load summaries
    sumdf = get_tou_summaries(intvldf)

    # Identify community sites (real product will pull data from elsewhere)
    sites = get_community_sites(intvldf, sumdf)

    # Get the estimated storage for each emergency site
    stordf = estimate_storage(intvldf, sites)

    print(stordf)
