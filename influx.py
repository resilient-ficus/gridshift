import csv
import glob
import datetime
import json
import gzip
import logging
import os
import time

import requests

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

BASE_URL = "http://127.0.0.1:8086/write?db=gridshift"

HEADERS = {}


def upload(out):
    response = requests.post(BASE_URL, data='\n'.join(out))
    data = response.content.decode('utf-8')
    print(response.content)

def process(filename):
    logging.info(filename)
    with gzip.open(filename, 'rt') as f:
        bulk = csv.reader(f)
        out = []
        header = None
        for i, row in enumerate(bulk):
            if i == 0:
                header = row
                print(header)
                continue
            # ['meter_uid', 'start', 'duration', 'kwh']
            line = 'bulk_500,meter_uid=%(meter_uid)d,duration=%(duration)d kwh=%(kwh)f %(start)s'

            ts = '%d000000000' % (int(row[1]) - (8 * 60 * 60))
            out.append(
                line % {
                    'meter_uid': int(row[0]),
                    'duration': int(row[2]),
                    'kwh': float(row[3]),
                    'start': ts,
                })
            if (i % 50000) == 0:
                try:
                    upload(out)
                except Exception as e:
                    logging.error('upload error: %i, %s', i, e)
                out = []
        upload(out)


def main():
    for filename in glob.iglob('data/bulk_intervals_500.csv.gz'):
        try:
            process(filename)
        except Exception as e:
            logging.error(e)


if __name__ == '__main__':
    main()
