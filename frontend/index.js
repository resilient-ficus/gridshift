var map,geocoder,service,markers,bounds,infoWindow;

function markers() {
  var iconBase = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/';

  var icons = {
    parking: {
      icon: iconBase + 'parking_lot_maps.png'
    },
    library: {
      icon: iconBase + 'library_maps.png'
    },
    center: {
      icon: 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle_highlight.png'
    },
    senior: {
      icon: 'http://maps.google.com/mapfiles/kml/pal2/icon28.png'
    },
    community: {
      icon: 'http://maps.google.com/mapfiles/kml/pal3/icon23.png'
    },
    church: {
      icon: 'http://maps.google.com/mapfiles/kml/pal2/icon11.png'
    },
    teen: {
      icon: 'http://maps.google.com/mapfiles/kml/pal2/icon49.png'
    },
    info: {
      icon: iconBase + 'info-i_maps.png'
    }
  };


  var features = [];

  function makeMarkers() {
      for (var i = 0; i < features.length; i++) {
        var marker = new google.maps.Marker({
          position: features[i].position,
          icon: icons[features[i].type].icon,
          map: map,
          title: features[i].title
        });
      }
  }
  fetch('data/data.json')
    .then((response) => {
      return response.json();
    })
    .then((sites) => {
      console.log(sites)
      site = 'Sunnyvale'
      data = sites['Sunnyvale']
      // for (const [site, data] of Object.entries(sites)) {
	    /*
        features.push({
          position: new google.maps.LatLng(data.location.lat, data.location.lng),
          type: 'center',
          title: site
        });
	*/

	    /*
        data.senior.forEach(e => {
          features.push({
            position: new google.maps.LatLng(e.location.lat, e.location.lng),
            type: 'senior',
            title: e.name
          });
        });
	*/
        data.community.sort(() => 0.5 - Math.random()).slice(0, 3).forEach(e => {
          features.push({
            position: new google.maps.LatLng(e.location.lat, e.location.lng),
            type: 'community',
            title: e.name
          });
        });
	    /*
        data.teen.forEach(e => {
          features.push({
            position: new google.maps.LatLng(e.location.lat, e.location.lng),
            type: 'teen',
            title: e.name
          });
        });*/
        data.church.sort(() => 0.5 - Math.random()).slice(0, 5).forEach(e => {
          features.push({
            position: new google.maps.LatLng(e.location.lat, e.location.lng),
            type: 'church',
            title: e.name
          });
        });
      //}

      makeMarkers();


    });
  // end fetch
  fetch('data/cities_polygons.json')
	.then((response) => {
		return response.json();
	})
	.then((data) => {
		svl_geojson = data['Sunnyvale']
		console.log(svl_geojson)
		map.data.addGeoJson({'type': 'Feature', 'geometry': svl_geojson})
	})

};


function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: { "lat": 37.36883, "lng": -122.0363496},
    zoom: 13
  });
  markers();

};

//getFeatures();
//markers();
